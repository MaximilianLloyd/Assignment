/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'
class TeacherController{
    constructor() {
        this.teacherRepository = require('./TeacherRepository.js');
    }

    getTechers(req, res) {
        this.teacherRepository.getTeachers().then(teachers => {
            res.json(teachers);
        });
    }



    getTeacherByUser (req, res) {
        let username = req.params.username;
        console.log('req.params.username', username);
        this.teacherRepository.getParentByUser(username).then(teacher => {
            console.log(JSON.stringify(teacher, null, 2));
            res.json(teacher);
        }).catch(err => {
            res.status(404).send("Not Found:" + err);
        });
    }



}

module.exports = new TeacherController();
