/**
 * Created by Fahad on 4/25/2016.
 */
'use strict'

let Announcement = require('./models/announcement.model');

class AnnoucementRepository {
    constructor() {
        this.fs = require('fs');
    }

    getAnnoucements() {
        return new Promise((resolve, reject) => {
            Announcement.find({}, (err, announcements) => {
              if(err) reject(err);
              else resolve(announcements)
            });
        });
    }


    addAnnoucement(annoucement) {
        return new Promise((resolve, reject) => {
          this.getAnnoucements().then(annoucements => {
                let maxOfId = Math.max.apply(Math, annoucements.map(a => a.aId)) + 1;
                console.log("maxOfId", maxOfId);
                annoucement.aId = maxOfId;
                console.log("added Annoucement", annoucement);
                Announcement.create(announcement, (err, data) => {
                  if(err) throw err;
                });

            }).then(()=> resolve(annoucement))
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }


    getAnnoucementById(aId) {
        return new Promise((resolve, reject) => {
            this.getAnnoucements().then(annoucements => {
                annoucements = annoucements.filter(annoucment => annoucment.aId === aId);
                if (annoucements.length > 0) {
                    resolve(annoucements[0]);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }


}

module.exports= new AnnoucementRepository();
