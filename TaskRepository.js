/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'

let Task = require('./models/task.model');

class TaskRepository {
    constructor() {
        this.fs = require('fs');
    }

    getTasks() {
        return new Promise((resolve, reject) => {
          Task.find({}, (err, tasks) => {
    if (err) reject(err);
    else resolve(tasks)
});
        });
    }

    getTaskByStudentId(studentId) {
        return new Promise((resolve, reject) => {
            this.getTasks().then(tasks => {
                tasks = tasks.filter(t => t.studentId === studentId);
                if (tasks.length > 0) {
                    resolve(tasks);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }
    getTaskByType(type) {
        return new Promise((resolve, reject) => {
            this.getTasks().then(tasks => {
                if (type=="Completed")
                {tasks = tasks.filter(t => t.completedDate);}
                else
                {tasks = tasks.filter(t => !(t.completedDate));}

                if (tasks.length > 0) {
                    resolve(tasks);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }
    getTaskById(taskId) {
        return new Promise((resolve, reject) => {
            this.getTasks().then(tasks => {
                tasks= tasks.filter(t => t.taskId === taskId);
                if (tasks.length > 0) {
                    resolve(tasks[0]);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }
    getTaskOfStudentByType(studentId,type) {
        return new Promise((resolve, reject) => {
            this.getTaskByStudentId(studentId).then(tasks => {

                if (type=="Completed")
                {tasks = tasks.filter(t => t.completedDate);}
                else
                {tasks = tasks.filter(t => !(t.completedDate));}
                if (tasks.length > 0) {
                    resolve(tasks);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }


    addTask(task) {
        return new Promise((resolve, reject) => {
            this.getTasks().then(tasks => {
                //Get the last Id used +1
                let maxId = Math.max.apply(Math, tasks.map(t => t.taskId)) + 1;
                console.log("maxId", maxId);

                task.taskId = maxId;

                console.log("addTask", task);

                Task.create(task, (err, task) => {
                  if(err) reject(err);
                  reolve(task);
                  console.log(chalk.green(`Successfully created task with id: ${task.taskId}`))
                });
            }).then(()=> resolve(task))
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }

    updateTask(task) {
        console.log( "task to be update is" +JSON.stringify (task));
        return new Promise((resolve, reject) => {
            Task.find({
              taskId: task.taskId
            }, (err, old_task) => {
              if(err) reject(err);
              old_task = task;
              old_task.save((err) => {
                if(err) reject(err);
                console.log(chalk.green(`Successfully updated task with id: ${task.taskId}`));
              });
            });
        });
    }

    deleteTask(taskId) {
        return new Promise((resolve, reject) => {
          Task.remove({
            taskId: taskId
          }, (err, task) => {
            if(err) reject(err);
            resolve(task);
          });
        });
    }

}
module.exports= new TaskRepository();
