'use strict';

let mongoose = require('mongoose');

let Surah = mongoose.model('Surah', {
  id: Number,
  name: String,
  englishName: String,
  ayaCount: Number,
  type: String
});

module.exports = Surah;
