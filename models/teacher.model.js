'use strict';

let mongoose = require('mongoose');

let Teacher = mongoose.model('Teacher', {
  staffNo: Number,
  username: String,
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  isCoordinator: Number
});

module.exports = Teacher;
