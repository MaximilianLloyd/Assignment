'use strict';

let mongoose = require('mongoose');

let Task = mongoose.model('Task', {
  taskId: Number,
  studentId: Number,
  surahId: Number,
  SurahName: String,
  fromAya: Number,
  toAya: Number,
  type: String,
  dueDate: String,
  completedDate: String,
  masteryLEvel: String,
  comment: String
});

module.exports = Task;
