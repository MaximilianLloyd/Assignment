'use strict';

let mongoose = require('mongoose');

let Student = mongoose.model('Student', {
  qatariId: Number,
  firstName: String,
  lastName: String,
  mobile: String,
  email: String,
  username: String,
  password: String,
  students: Array
});

module.exports = Student;
