'use strict';

let mongoose = require('mongoose');

let Announcment = mongoose.model('Announcment', {
  aId: Number,
  title: String,
  message: String
});

module.exports = Announcment;
