'use strict';

let mongoose = require('mongoose');

let Message = mongoose.model('Message', {
  messageId: Number,
  qatariId: Number,
  title: String,
  message: String
});

module.exports = Message;
