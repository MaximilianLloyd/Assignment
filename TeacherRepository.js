/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'

let Teacher = require('./models/teacher.model');

class TeacherRepository {
    constructor() {
        this.fs = require('fs');
    }

    getTeachers() {
        return new Promise((resolve, reject) => {
            Teacher.find({}, (err, tasks) => {
                if (err) reject(err);
                else resolve(tasks)
            });
        });
    }

    getTeacherByUser(username) {
        return new Promise((resolve, reject) => {
            this.getTeachers().then(teachers => {
                teachers = teachers.filter(t => t.username === username);
                if (teachers.length > 0) {
                    resolve(teachers[0]);
                } else {
                    reject("Not Found:");
                }
            });
        });
    }


    authenticate(user, password) {
        return new Promise((resolve, reject) => {
            this.getTeachers().then(teachers => {
                teachers = teachers.filter(t => t.username === user && t.password == password);
                if (teachers.length > 0) {
                    resolve(teachers[0]);
                } else {
                    reject("Not Found:");
                }
            });
        });
    }

}
module.exports = new TeacherRepository();
