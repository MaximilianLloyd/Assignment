/**
 * Created by Fahad on 4/24/2016.
 */
"use strict";
let express = require('express'),
    mongoose = require('mongoose'),
    config = require('./config'),
    chalk = require('chalk'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    Announcement = require('./models/announcement.model'),
    Message = require('./models/message.model'),
    Student = require('./models/student.model'),
    Surah = require('./models/surah.model'),
    Task = require('./models/task.model'),
    Teacher = require('./models/teacher.model');

if (config.development) {
    // Working locally
    mongoose.connect('mongodb://localhost:27017/assignment', (err) => {
        if (err) {
            console.log(chalk.red('Failed to connect to mongodb'));
            throw err;
        }

        console.log(chalk.green('successfully connected to mongodb'));
    });
} else {
    // Production
}


// If one of them is empty the rest of them will also be unless someone has deleted the whole collection manually
Announcement.find({}, (err, announcements) => {
    if (err) throw err;

    // Start seeding process
    if (!announcements.length) {
        console.log(chalk.blue('Database is empty, starting seeding process'));

        // Announcments
        fs.readFile('./data/annoucement.json', (err, data) => {
            if (err) throw err;
            let announcements = JSON.parse(data);
            for (let i = 0; i < announcements.length; i++) {
                Announcement.create(announcements[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });

        fs.readFile('./data/message.json', (err, data) => {
            if (err) throw err;
            let messages = JSON.parse(data);
            for (let i = 0; i < messages.length; i++) {
                Message.create(messages[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });

        fs.readFile('./data/student.json', (err, data) => {
            if (err) throw err;
            let students = JSON.parse(data);
            for (let i = 0; i < students.length; i++) {
                Student.create(students[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });

        fs.readFile('./data/surah.json', (err, data) => {
            if (err) throw err;
            let surahs = JSON.parse(data);
            for (let i = 0; i < surahs.length; i++) {
                Surah.create(surahs[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });

        fs.readFile('./data/task.json', (err, data) => {
            if (err) throw err;
            let tasks = JSON.parse(data);
            for (let i = 0; i < tasks.length; i++) {
                Task.create(tasks[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });

        fs.readFile('./data/teacher.json', (err, data) => {
            if (err) throw err;
            let teachers = JSON.parse(data);
            for (let i = 0; i < teachers.length; i++) {
                Teacher.create(teachers[i], (err, data) => {
                    if (err) throw err;
                });
            }
        });
    }
});



let app = express();
app.use(express.static(__dirname));

let port = 9090;

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


let studentController = require('./StudentController');
let studentRepository = require('./StudentRepository');
let taskController = require('./TaskController');
let teacherController = require('./TeacherController');
let teacherRepository = require('./TeacherRepository');
let surahController = require('./SurahController');
let messageController = require('./MessageController');
let annoucementController = require('./AnnoucementController');

app.get('/', (req, res) => res.redirect('Home.html'));
app.post('/', (req, res) => {
    let userInfo = req.body;
    console.log(userInfo);
    let count = 0;

    studentRepository.getParents().then(parents => {
        for (let i = 0; i < parents.length; i++) {
            if (parents[i].username == userInfo.username && parents[i].password == userInfo.password) {
                console.log('Is parent');
                let parent = parents[i];
                count++;

                // HACK: Have to cheat the system because it is read only :(
                let ret = {
                  password   : parent.password,
                  username   : parent.username,
                  email      : parent.email,
                  mobile     : parent.mobile,
                  lastName   : parent.lastName,
                  firstName  : parent.firstName,
                  qatariId   : parent.qatariId,
                  redirectTo : '/Parent.html',
                  students   : parent.students
                };

                console.log(ret);
                res.json(ret);
                break;
            }
        }
    });
    teacherRepository.getTeachers().then(teachers => {
        for (let i = 0; i < teachers.length; i++) {
            console.log(userInfo);

            if (teachers[i].username == userInfo.username && teachers[i].password == userInfo.password) {
                console.log('Is Teacher');
                let teacher = teachers[i];

                let ret = {
                  password   : teacher.password,
                  username   : teacher.username,
                  email      : teacher.email,
                  mobile     : teacher.mobile,
                  lastName   : teacher.lastName,
                  firstName  : teacher.firstName,
                  qatariId   : teacher.qatariId,
                  redirectTo : '/Parent.html',
                  students   : teacher.students
                };

                count++;
                if (teachers[i].isCoordinator) {
                    ret.redirectTo = '/Coordinator.html';
                    console.log('Is Coordinator');
                } else {
                    ret.redirectTo = '/Instructor.html';
                    console.log('Is Instructor');
                }

                console.log(ret);
                res.json(ret);
                break;

            } else if (count == 0 && i == (teachers.length - 1)) {
                res.json(ret);
            }
        }
    });
});

app.get('/api/parents', (req, res) => studentController.getParents(req, res));

app.get('/api/tasks', (req, res) => taskController.getTasks(req, res));
app.post('/api/tasks', (req, res) => taskController.addTask(req, res));
app.put('/api/tasks/:taskId', (req, res) => taskController.updateTask(req, res));
app.delete('/api/tasks/:taskId', (req, res) => taskController.deleteTask(req, res));

app.get('/api/tasks/:taskId', (req, res) => taskController.getTask(req, res));

app.get('/api/task/:type', (req, res) => taskController.getTaskByType(req, res));
app.get('/api/t/:studentId', (req, res) => taskController.getTaskByStudentId(req, res));

app.get('/api/t/:studentId/:type', (req, res) => taskController.getTaskByStudentIdType(req, res));
app.get('/api/surahs/:id', (req, res) => surahController.getSurah(req, res));
app.get('/api/surahs', (req, res) => surahController.getSurahs(req, res));
app.get('/api/annoucements', (req, res) => annoucementController.getAnnoucements(req, res));

app.get('/api/annoucements/:aId', (req, res) => annoucementController.getAnnoucement(req, res));
app.get('/api/messages', (req, res) => messageController.getMessages(req, res));
app.get('/api/messages/:messageId', (req, res) => messageController.getMessage(req, res));
app.get('/api/teachers/:username', (req, res) => teacherController.getTeacherByUser(req, res));
app.get('/api/teachers', (req, res) => teacherController.getTechers(req, res));
app.get('/api/students', (req, res) => studentController.getStudents(req, res));
app.get('/api/parentschild/:username', (req, res) => studentController.getStudentOfParent(req, res));

app.get('/api/parents', (req, res) => studentController.getParents(req, res));
app.get('/api/parents/:username', (req, res) => studentController.getParentByUN(req, res));

app.get('/api/surahs', (req, res) => surahController.getSurahs(req, res));


app.listen(port, function() {
    console.log('Students App is running my app on http://localhost:' + port);
});
