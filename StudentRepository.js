/**
 * Created by Fahad on 4/23/2016.
 */
'use strict'

let Student = require('./models/student.model');

class StudentRepository {
    constructor() {
        this.fs = require('fs');
    }

    getParents() {
        return new Promise((resolve, reject) => {
          Student.find({}, (err, students) => {
            if(err) reject(err);
            else resolve(students);
          });
        });
    }

    getParentByUser(username) {
        return new Promise((resolve, reject) => {
            this.getParents().then(parents => {
                parents = parents.filter(p => p.username === username);
                if (parents.length > 0) {
                    resolve(parents[0]);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }

    getStudents(){
        return new Promise((resolve, reject) => {
            this.getParents().then(parents => {
                let students=[];
                for(var i=0;i<parents.length;i++){
                    for(var j=0;j<parents[i].students.length;j++)
                    {
                        students.push(parents[i].students[j]);}
                }
                if (students.length > 0) {
                    resolve(students);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }
    getStudentOfParent(username){
        return new Promise((resolve, reject) => {
            Student.find({
              username: username
            }, (err, students) => {
              if(err) reject(err);
              resolve(students);
            });
        });
    }


    authenticate(user, password){
        return new Promise((resolve, reject) => {
            this.getParents().then(parents => {
                parents = parents.filter(t => t.username === user && t.password == password);
                if (parents.length > 0) {
                    resolve(parents[0]);
                }
                else {
                    reject("Not Found:");
                }
            });
        });
    }

}
module.exports= new StudentRepository();
