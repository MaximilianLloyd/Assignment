/**
 * Created by Fahad on 4/24/2016.
 */


'use strict'
class LoginController{
    constructor() {
        this.teacherRepository = require('./TeacherRepository.js');
        this.StudentRepository = require('./StudentRepository.js');
    }



    login (req, res) {
        let username = req.params.username;
        let password = req.params.password;

        console.log('req.params.username', username);
        this.teacherRepository.authenticate(username, password).then(teacher => {
            console.log(JSON.stringify(teacher, null, 2));
            res.json(teacher);
        }).catch(err => {
                this.StudentRepository.authenticate(username, password).then(student => {
                    console.log(JSON.stringify(student, null, 2));
                    res.json(student);
                }).catch(err => {
                    res.status(404).send("Not Found:" + err);
                });

        }

        );
    }

}

module.exports = new LoginController();
