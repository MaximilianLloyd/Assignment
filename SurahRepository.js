/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'

let Surah = require('./models/surah.model');

class SurahRepository {
    constructor() {
        this.fs = require('fs');
    }

    getSurahs() {
        return new Promise((resolve, reject) => {
          Surah.find({}, (err, surahs) => {
            if(err) reject(err);
            else resolve(surahs)
          });
        });
    }

    getSurahById(Id) {
        return new Promise((resolve, reject) => {
            this.getSurahs().then(surahs => {
                surahs = surahs.filter(s => s.id === Id);
                if (surahs.length > 0) {
                    resolve(surahs[0]);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }

    getNumberOfAya(type,ayaCount) {
        return new Promise((resolve, reject) => {
            this.getSurah(type).then(surahs => { surahs = surahs.filter(s => s.ayaCount>=ayaCount  );
                if (surahs.length > 0) {
                    resolve(surahs);
                }
                else {
                    reject("No records found");
                }
            })
        });
    }
}

module.exports = new SurahRepository();
