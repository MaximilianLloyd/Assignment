/**
 * Created by Fahad on 4/24/2016.
 */

'use strict'

let Message = require('./models/message.model');

class MessageRepository {
    constructor() {
        this.fs = require('fs');
    }


    getMessages() {
        return new Promise((resolve, reject) => {
          Message.find({}, (err, messages) => {
            if(err) reject(err);
            else resolve(messages)
          });
        });
    }


    getMessageById(messageId) {
        return new Promise((resolve, reject) => {
            this.getMessages().then(messages => {
                messages = messages.filter(m => m.messageId === messageId);
                if (messages.length > 0) {
                    resolve(messages[0]);
                }
                else {
                    reject("No records found");
                }
            });
        });
    }

    addMessage(message) {
        return new Promise((resolve, reject) => {
            this.getMessages().then(messages => {
                let maxId = Math.max.apply(Math, messages.map(m => m.messageId)) + 1;
                console.log("maxId", maxId);

                message.messageId = maxId;
                console.log("addMessage", message);
                Message.create(message, (err, data) => {
                  if(err) throw err;
                });

            }).then(()=> resolve(message))
                .catch(err => {
                    console.log(err);
                    reject(err);
                });
        });
    }



}
module.exports= new MessageRepository();
