/**
 * Created by Fahad on 4/23/2016.
 */
'use strict'
class StudentController{
    constructor() {
        this.studentRepository = require('./StudentRepository.js');
    }

    getStudents(req, res) {
        this.studentRepository.getStudents().then(students => {
            res.json(students);
        });
    }

    getParents(req, res) {
        this.studentRepository.getParents().then(parents => {
            res.json(parents);
        });
    }

    getParentByUser (req, res) {
        let username = req.params.username;
        console.log('req.params.username', username);
        this.studentRepository.getParentByUN(username).then(parent => {
            console.log(JSON.stringify(parent, null, 2));
            res.json(parent);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }
    getStudentOfParent (req, res) {
        let username = req.params.username;
        console.log('req.params.username', username);
        this.studentRepository.getStudentOfParent(username).then(student => {
            console.log(JSON.stringify(student, null, 2));
            res.json(student);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }

}

module.exports = new StudentController();
