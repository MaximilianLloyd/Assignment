/**
 * Created by Fahad on 4/24/2016.
 */
'use strict'
class MessageController {
    constructor() {
        this.messageRepository = require('./MessageRepository');
    }

    getMessages(req, res) {
        this.messageRepository.getMessages().then(messages => {
            res.json(messages);
        });
    }

    getMessage (req, res) {
        let messageId = req.params.messageId;
        console.log('req.params.messageId', messageId);
        this.messageRepository.getMessageById(parseInt(messageId)).then(message => {
            console.log(JSON.stringify(message, null, 2));
            res.json(message);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }


    addMessage(req, res) {
        let message = req.body;

        this.messageRepository.addMessage(message).then((message)=> {
                let urlOfNewMessage = `/api/messages/${message.messageId}`;
                res.location(urlOfNewMessage)
                res.status(201).send(`Created and available @ ${urlOfNewMessage}`);
            })
            .catch(err => res.status(500).send(err));
    }


}

module.exports = new MessageController();