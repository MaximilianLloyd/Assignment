/**
 * Created by Fahad on 4/24/2016.
 */

var selectedValue1= 1;
var selectedValue2= 1;
$(document).ready(function(){

});


function updateSpan1(){
    $("#selectedValue1").text(fromAya.value);
}
function updateSpan2(){
    $("#selectedValue2").text(toAya.value)
}
function getSurahs() {
    let url = "http://localhost:9090/api/surahs";
    return fetch(url).then(response => response.json());
}

function getSurah(surahId) {
    let url = `http://localhost:9090/api/surahs/${surahId}`;
    return fetch(url).then(response => response.json());
}

function updateSliders(surah) {

    $("#fromAya").val(1);
    $("#toAya").val(1);

    $("#fromAya").attr('max',surah.ayaCount);
    $("#toAya").attr('max',surah.ayaCount);

    /*
     let htmlTemplate = $('#student-template').html(),

     studentTemplate = Handlebars.compile(htmlTemplate)

     console.log('studentTemplate(student)', studentTemplate(student));

     $('#studentDetails').html(studentTemplate(student));
     */
}


function onSurahChange() {
    let selectedSurahId = $('#surahDD').val();

    getSurah(selectedSurahId).then(surah => {
        updateSliders(surah);
    }).catch(err => console.log(err));
}

function fillSurahsDD(surahs) {
    for (let surah of surahs) {
        $("<option>", {
            value: surah.id,
            text: surah.englishName
        }).appendTo($("#surahDD"))
    }
}

function fillStudentDD(students) {
    for (let student of students) {
        $("<option>", {
            value: student.studentId,
            text: student.firstName + " " + student.lastName
        }).appendTo($("#studentDD"))
    }
}

function displayTasks(tasks) {
    let htmlTemplate = $('#tasks-template').html(),
        TaskesTemplate = Handlebars.compile(htmlTemplate)

    $('#tasks-list').html(TaskesTemplate({tasks}));
}

function listCompletedTasks() {
    //Empty the Task-details div
    let teacher= JSON.parse(localStorage.user);
    $('#tasks-list').empty();
    let tt = [];
    fetchTasks().then(tasks => {
        tasks = tasks.filter(t=> t.completedDate !== undefined);
            fetchStudents().then( students => {
                students = students.filter (s=> s.teacherId === teacher.staffNo);
                tasks.forEach(t=> {
                    let student  = students.filter(s => s.studentId === t.studentId)[0];

                    if (student) {
                        t.studentName = student.firstName + " " + student.lastName;
                        t.whichList="completed";
                        tt.push(t);
                    }

                });
                displayTasks(tt);
            });


        })
        .catch(err => console.log(err));
}

function listPendingTasks() {
    //Empty the Task-details div
    let teacher= JSON.parse(localStorage.user);
    $('#tasks-list').empty();
    let tt = [];
    fetchTasks().then(tasks => {
            tasks = tasks.filter(t=> t.completedDate === undefined);
            fetchStudents().then( students => {
                students = students.filter (s=> s.teacherId === teacher.staffNo);
                tasks.forEach(t=> {
                    let student  = students.filter(s => s.studentId === t.studentId)[0];

                    if (student) {
                        t.studentName = student.firstName + " " + student.lastName;
                        t.whichList="pending";

                        tt.push(t);
                    }

                });
                displayTasks(tt);
            });


        })
        .catch(err => console.log(err));
}

    function listAllTasks() {
        //Empty the Task-details div
        let teacher= JSON.parse(localStorage.user);
        $('#tasks-list').empty();
        let tt = [];

        fetchStudents().then( students => {
            students = students.filter (s=> s.teacherId === teacher.staffNo);

            fetchTasks().then(tasks => {

                    tasks.forEach(t=> {

                        let student = students.filter(s => s.studentId === t.studentId)[0];
                        console.log(JSON.stringify(student));
                        if (student) {
                            t.studentName = student.firstName + " " + student.lastName;
                            t.whichList="all";
                            tt.push(t);

                        }


                    });
                    displayTasks(tt);
                });


            })
            .catch(err => console.log(err));
    }

    function fetchTasks() {
        let url = `http://localhost:9090/api/tasks`;
        return fetch(url).then(response => response.json());
    }

function fetchStudents() {
    let url = `http://localhost:9090/api/students`;
    return fetch(url).then(response => response.json());
}






   function fetchTask(tId) {
        let url = `http://localhost:9090/api/tasks/${tId}`;
        return fetch(url).then(response => response.json());
    }





function updateTask(TaskId) {
    $('#tasks-list').empty();

    console.log("TaskId", TaskId);
    let teacher= JSON.parse(localStorage.user);
    fetchTask(TaskId).then(Task => {
        console.log(Task);

        let htmlTemplate = $('#task-form-template').html(),
            TaskTemplate = Handlebars.compile(htmlTemplate);

        $('#task-form').html(TaskTemplate(Task));
        getSurahs().then(surahs => {
            fillSurahsDD(surahs)
                $('#surahDD').val(Task.surahId);
                let selectedSurahId = $('#surahDD').val();

                getSurah(selectedSurahId).then(surah => {
                    updateSliders(surah);

                    $('#fromAya').val(Task.fromAya);
                    $('#toAya').val(Task.toAya);
                }).catch(err => console.log(err));


                $('#dd').val(Task.dueDate);
                 $('#taskId').val(Task.taskId);

                RadionButtonSelectedValueSet('memo',Task.type);
                 // $('#memo').val(Task.type);

                $("#surahDD").on('change', onSurahChange);



                $("#fromAya").on('input',updateSpan1);
                $("#toAya").on('input',updateSpan2);



            })
            .catch(err => console.log(err));

        fetchStudents().then(students => {
                students = students.filter(s => s.teacherId === teacher.staffNo);
                fillStudentDD(students);
                $('#studentDD').val(Task.studentId);

            })
            .catch(err => console.log(err));

        //Select the TaskType in the Dropdown
        //$('#surahDD').val(Task.surahId);
//       // $('#studentDD').change();
       // onSurahChange();


        showFormAsModel();

    }).catch(err => console.log(err));
}

function addTask() {
    let teacher= JSON.parse(localStorage.user);

    $('#tasks-list').empty();
    let htmlTemplate = $('#task-form-template').html(),
        taskTemplate = Handlebars.compile(htmlTemplate);

    $('#task-form').html(taskTemplate({}));

    getSurahs().then(surahs => fillSurahsDD(surahs))
        .catch(err => console.log(err));

    fetchStudents().then(students => {
            students = students.filter(s => s.teacherId === teacher.staffNo);
            fillStudentDD(students)
        })
        .catch(err => console.log(err));

    $("#surahDD").on('change', onSurahChange);



    $("#fromAya").on('input',updateSpan1);
    $("#toAya").on('input',updateSpan2);



    var now = new Date();
    now.setDate(now.getDate()+1);
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('#dd').val(today);
    console.log(today);
    showFormAsModel();
}

function showFormAsModel() {



    let taskForm = $("#task-form").dialog({
        height: 510,
        width: 750,
        title: 'Add a new Task ',
        modal: true,
        buttons: {
            "Submit": function() {
                saveTask();
                taskForm.dialog( "close" );
            },
            Cancel: function() {
                taskForm.dialog( "close" );
            }
        }
    });
}

function deleteTask(taskId,whichList) {


    // Ask the user to confirm. If they cancel the request then exit this function
    if (!confirm('Confirm delete?')) {
        return;
    }

    //Get the data-TaskId custom attribute associated with the clicked Link
    //Note this refers to the link that was clicked (i.e., the source of the click event)


    let url = `http://localhost:9090/api/tasks/${taskId}`;
    console.log("deletteTask.TaskId", taskId);
    fetch(url, {method: 'delete'}).then(() => {
        //After successful delete remove the row from the HTML table
        $(this).closest('tr').remove();
    }).then(() => {
        //After delete then refresh the list
        if(whichList === "all"){
            listAllTasks();
        }
        else if(whichList === "completed"){
            listCompletedTasks();

        }
        else if(whichList === "pending"){
            listPendingTasks();

        }
    });
}
function saveTask() {

    let Task = {
        studentId: parseInt( $('#studentDD').val()),
        surahId: parseInt($('#surahDD').val()),
        surahName: $('#surahDD option:selected').text(),
        fromAya: parseInt($('#fromAya').val()),
        toAya: parseInt( $('#toAya').val()),
        type: $('input[name="memo"]:checked').val(),
        dueDate: $('#dd').val()


    };

    let url = "http://localhost:9090/api/tasks/";
    let requestMethod = "post";

    let TaskId = $('#taskId').val();

    //In case of update make the method put and append the id to the Url
    if (TaskId != '') {
        Task.taskId = parseInt(TaskId);
        url += TaskId;
        requestMethod = "put";
    }

    console.log("saveTask.TaskId", TaskId);

    fetch(url, {
        method: requestMethod,
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify( Task)
    }).then(() => {
        //After add/update then refresh the list
        listAllTasks();
    });


}

function RadionButtonSelectedValueSet(name, SelectdValue) {
    $('input[name="' + name+ '"][value="' + SelectdValue + '"]').prop('checked', true);
}
