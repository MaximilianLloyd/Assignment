/**
 * Created by Fahad on 4/23/16.
 */

'use strict';

 $(document).ready(function () {
     $('#login').on('click', loginForm);
 });

function loginForm(event) {
    event.preventDefault();

    let userInfo = {
        username: $('#username').val(),
        password: $('#password').val(),
    };
    console.log("loginForm.userInfo", userInfo);

    let url = "http://localhost:9090/";

    fetch(url, {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(userInfo)
    }).then(response => response.json())
        .then(user => {

            if (user.redirectTo){
                console.log("Redirect to*****:", JSON.stringify(user));
                localStorage.user = JSON.stringify(user);

                window.location = user.redirectTo;

            }
            else{
                alert('login failed');
            }
        });
}
