/**
 * Created by Fahad on 4/28/2016.
 */



$(document).ready(function() {
    //let parent= JSON.parse(localStorage.user);
    listChildrenTasks();

});


function getSurahs() {
    let url = "http://localhost:9090/api/surahs";
    return fetch(url).then(response => response.json());
}

function getSurah(surahId) {
    let url = `http://localhost:9090/api/surahs/${surahId}`;
    return fetch(url).then(response => response.json());
}



function displayTasks(tasks) {
    let htmlTemplate = $('#tasks-template').html(),
        TaskesTemplate = Handlebars.compile(htmlTemplate)

    $('#tasks-list').html(TaskesTemplate({
        tasks
    }));
}

function listChildrenTasks() {
    //Empty the Task-details div
    let parent = JSON.parse(localStorage.user);
    $('#tasks-list').empty();
    let tt = [];
    fetchTasks().then(tasks => {
            let students = parent.students;
            tasks.forEach(t => {
                let student = students.filter(s => s.studentId === t.studentId)[0];

                if (student) {
                    t.studentName = student.firstName + " " + student.lastName;

                    tt.push(t);
                }

            });
            displayTasks(tt);



        })
        .catch(err => console.log(err));
}

function fetchTasks() {
    let url = `http://localhost:9090/api/tasks`;
    return fetch(url).then(response => response.json());
}
