/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'
class TaskController {
    constructor() {
        this.taskRepository = require('./TaskRepository');
    }

    getTasks(req, res) {
        this.taskRepository.getTasks().then(tasks => {
            res.json(tasks);
        });
    }

    getTask (req, res) {
        let taskId = req.params.taskId;
        console.log('req.params.taskId', taskId);
        this.taskRepository.getTaskById(parseInt(taskId)).then(task => {
            console.log(JSON.stringify(task, null, 2));
            res.json(task);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }
    getTaskByStudentId (req, res) {
        let studentId = req.params.studentId;
        console.log('req.params.studentId', studentId);
        this.taskRepository.getTaskByStudentId(parseInt(studentId)).then(task => {
            console.log(JSON.stringify(task, null, 2));
            res.json(task);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }
    getTaskByStudentIdType (req, res) {
        let studentId = req.params.studentId;
        let type = req.params.type;

        console.log('req.params.studentId', studentId);
        console.log('req.params.type', type);

        this.taskRepository.getTaskOfStudentByType(parseInt(studentId),type).then(task => {
            console.log(JSON.stringify(task, null, 2));
            res.json(task);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }
    getTaskByType (req, res) {
        let type = req.params.type;
        console.log('req.params.', type);
        this.taskRepository.getTaskByType(type).then(task => {
            console.log(JSON.stringify(task, null, 2));
            res.json(task);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }



    addTask(req, res) {
        let task = req.body;

        this.taskRepository.addTask(task).then((task)=> {
                let urlOfNewTask = `/api/tasks/${task.taskId}`;
                res.location(urlOfNewTask)
                res.status(201).send(`Created and available @ ${urlOfNewTask}`);
            })
            .catch(err => res.status(500).send(err));
    }

    updateTask(req, res) {
        let task = req.body;

        this.taskRepository.updateTask(task).then(() => {
            res.status(200).send("task updated successfully");
        }).catch(err => {
            console.log(err);
            res.status(500).send(err);
        });
    }

    deleteTask(req, res) {
        let taskId = req.params.taskId;

        this.taskRepository.deleteTask(taskId).then(() => {
            res.status(200).send("Task deleted");
        }).catch(err => {
            res.status(500).send(err);
        });
    }
}

module.exports = new TaskController();