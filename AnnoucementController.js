/**
 * Created by Fahad on 4/25/2016.
 */
'use strict'

class AnnoucementController {
    constructor() {
        this.annoucementRepository = require('./AnnoucementRepository.js');
    }

    getAnnoucements(req, res) {
        this.annoucementRepository.getAnnoucements().then(annoucements => {
            res.json(annoucements);
        });
    }

    getAnnoucement (req, res) {
        let annId = req.params.aId;
        console.log('req.params.annId', annId);
        this.annoucementRepository.getAnnoucementById(parseInt(annId)).then(annoucement => {
            console.log(JSON.stringify(annoucement, null, 2));
            res.json(annoucement);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }


    addAnnoucement(req, res) {
        let annoucement = req.body;

        this.annoucementRepository.addAnnoucement(annoucement).then((annoucement)=> {
                let url= `/api/annoucements/${annoucement.aId}`;
                res.location(url)
                res.status(201).send(`Announcment added sucsessfully @ ${url}`);
            }).catch(err => res.status(500).send(err));
    }


}

module.exports = new AnnoucementController();