/**
 * Created by Fahad on 4/22/2016.
 */
'use strict'
class SurahController{
    constructor() {
        this.surahRepository = require('./SurahRepository');
    }

    getSurahs(req, res) {
        this.surahRepository.getSurahs().then(surahs => {
            res.json(surahs);
        });
    }
    getSurah (req, res) {
        let Id = req.params.id;
        this.surahRepository.getSurahById(parseInt(Id)).then(surah => {
            res.json(surah);
        }).catch(err => {
            res.status(404).send("Failed :" + err);
        });
    }


}

module.exports = new SurahController();